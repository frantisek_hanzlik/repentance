use crate::structures::Node;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Xml<'a>(&'a Node)
);

impl Display for Xml<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match **self {
			Node::Element(inner) => write!(f, "{}", inner.display_xml()),
			Node::Literal(inner) => write!(f, "{}", inner.display_xml()),
		}
	}
}
