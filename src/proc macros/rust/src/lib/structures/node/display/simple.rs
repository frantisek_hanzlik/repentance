use crate::structures::Node;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Simple<'a>(&'a Node)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match **self {
			Node::Element(inner) => write!(f, "{}", inner.display_simple()),
			Node::Literal(inner) => write!(f, "{}", inner.display_simple()),
		}
	}
}
