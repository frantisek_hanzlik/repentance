mod display;

use super::{literal::Literal, Element};
use proc_macro2::TokenStream;
use quote::ToTokens;
use syn::{parse::Parse, Ident};

#[derive(Debug)]
pub enum Node {
	Element(Element),
	Literal(Literal),
}

impl Node {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl Parse for Node {
	fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
		if input.peek(Ident) {
			Ok(Self::Element(input.parse()?))
		} else if input.peek(syn::Lit) {
			Ok(Self::Literal(input.parse()?))
		} else {
			Err(input.error("Unexpected token - expected either an element or a literal"))
		}
	}
}

impl ToTokens for Node {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		match self {
			Self::Element(inner) => inner.to_tokens(tokens),
			Self::Literal(inner) => inner.to_tokens(tokens),
		}
	}
}
