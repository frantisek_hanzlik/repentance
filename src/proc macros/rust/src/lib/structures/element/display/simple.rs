use crate::structures::Element;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Simple<'a>(&'a Element)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"{}{}{}",
			self.name.to_string(),
			self.attributes.display_simple(),
			self.children.display_simple(),
		)?;

		Ok(())
	}
}
