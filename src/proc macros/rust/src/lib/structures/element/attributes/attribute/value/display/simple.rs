use crate::structures::element::attributes::attribute::value::Value;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Simple<'a>(&'a Value)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match **self {
			Value::Str(inner) => write!(f, "\"{}\"", inner.value()),
			Value::Bool(inner) => write!(f, "{}", inner.value),
			Value::Int(inner) => write!(f, "{}", inner),
			Value::Float(inner) => write!(f, "{}", inner),
		}
	}
}
