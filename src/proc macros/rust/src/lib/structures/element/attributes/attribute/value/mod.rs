mod display;

use std::convert::TryFrom;
use syn::{Lit, LitBool, LitFloat, LitInt, LitStr};

#[derive(Debug)]
pub enum Value {
	Str(LitStr),
	Bool(LitBool),
	Int(LitInt),
	Float(LitFloat),
}

impl Value {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl TryFrom<Lit> for Value {
	type Error = syn::Error;

	fn try_from(value: Lit) -> Result<Self, Self::Error> {
		match value {
			Lit::Str(inner) => Ok(Self::Str(inner)),
			Lit::Bool(inner) => Ok(Self::Bool(inner)),
			Lit::Int(inner) => Ok(Self::Int(inner)),
			Lit::Float(inner) => Ok(Self::Float(inner)),
			rest => Err(syn::Error::new(
				rest.span(),
				"Only string, number and boolean literals are allowed here",
			)),
		}
	}
}
