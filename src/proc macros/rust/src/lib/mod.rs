#![feature(proc_macro_diagnostic)]
#![feature(debug_non_exhaustive)]
#![feature(type_ascription)]
#![feature(decl_macro)]
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required during release
// #![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]

mod codegen;
mod structures;

use proc_macro::TokenStream;
use quote::{quote, ToTokens};
use structures::Node;
use syn::parse_macro_input;

#[proc_macro]
pub fn xml(input: TokenStream) -> TokenStream {
	let node = parse_macro_input!(input as Node);

	let node_as_string = format!("{}", node.display_xml());

	let tokens = quote!(#node_as_string);

	tokens.into()
}

#[proc_macro]
pub fn xml_runtime(input: TokenStream) -> TokenStream {
	let node = parse_macro_input!(input as Node);

	node.to_token_stream().into()
}
