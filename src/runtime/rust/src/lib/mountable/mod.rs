pub mod impls;

use web_sys::Node;

pub trait Mountable {
	fn mount(&self, parent: &Node);
}

impl<T, const N: usize> Mountable for [&T; N]
where
	T: Mountable + ?Sized,
{
	fn mount(&self, parent: &Node) { self.iter().for_each(|item| item.mount(parent)); }
}
